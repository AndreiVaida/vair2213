package vair2213MV;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.IntrebareValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import evaluator.repository.IntrebariRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.FileWriter;

import static org.junit.Assert.assertEquals;

public class IntegrationTest_TopDown {
    private IntrebariRepository intrebariRepository;
    private AppController appController;
    private String fileName = "test.txt";

    @Before
    public void init() throws Exception {
        intrebariRepository = new IntrebariRepository();
        BufferedWriter br = new BufferedWriter(new FileWriter(fileName));
        br.write("");
        br.close();
        appController = new AppController();
    }

    @Test
    public void unitTest_F01() throws IntrebareValidationFailedException, DuplicateIntrebareException {
        // add Intrebare
        intrebariRepository.addIntrebare(new Intrebare("Întrebare 1 ?", "1) R1", "2) R2.", "1", "Mate"), fileName);
        intrebariRepository.addIntrebare(new Intrebare("Întrebare 2 ?", "1) R1", "2) R2.", "2", "Mate"), fileName);
        assertEquals(2, intrebariRepository.getIntrebari().size());
    }

    @Test
    public void unitTest_F02() throws IntrebareValidationFailedException, DuplicateIntrebareException {
        // create Test
        // init: 5î + 5d
        intrebariRepository.addIntrebare(new Intrebare("Enunț 1?", "1) v1", "2) v2", "1", "Matematică"), fileName);
        intrebariRepository.addIntrebare(new Intrebare("Enunț 2?", "1) v1", "2) v2", "1", "Română"), fileName);
        intrebariRepository.addIntrebare(new Intrebare("Enunț 3?", "1) v1", "2) v2", "1", "Info"), fileName);
        intrebariRepository.addIntrebare(new Intrebare("Enunț 4?", "1) v1", "2) v2", "1", "Biologie"), fileName);
        intrebariRepository.addIntrebare(new Intrebare("Enunț 5?", "1) v1", "2) v2", "1", "Chimie"), fileName);
        assertEquals(5, intrebariRepository.getIntrebari().size());
        assertEquals(5, intrebariRepository.getNumberOfDistinctDomains());
        // test
        try {
            final evaluator.model.Test test = appController.createNewTest(fileName);
            Assert.assertEquals(5, test.getIntrebari().size());
        } catch (NotAbleToCreateTestException e) {
            Assert.fail();
        }
    }

    @Test
    public void unitTest_F03() throws IntrebareValidationFailedException, DuplicateIntrebareException {
        // get Statistica
        // populate the file with questions in order to do the statistics
        appController.addNewIntrebare(new Intrebare("Întrebare 1?", "1) R1", "2) R2.", "1", "Mate"), fileName);
        appController.addNewIntrebare(new Intrebare("Întrebare 2?", "1) R1", "2) R2.", "2", "Mate"), fileName);
        appController.addNewIntrebare(new Intrebare("Întrebare 3?", "1) R1", "2) R2.", "1", "Info"), fileName);
        appController.addNewIntrebare(new Intrebare("Întrebare 4?", "1) R1", "2) R2.", "2", "Info"), fileName);
        appController.addNewIntrebare(new Intrebare("Întrebare 5?", "1) R1", "2) R2.", "1", "Chimie"), fileName);

        // test
        try {
            final Statistica statistica = appController.getStatistica();
            Assert.assertNotNull(statistica);
        } catch (NotAbleToCreateStatisticsException e) {
            Assert.fail();
        }
    }

    @Test
    public void integrationTest_TopDown_A() throws IntrebareValidationFailedException, DuplicateIntrebareException {
        // test Add Intrebare
        appController.addNewIntrebare(new Intrebare("Enunț 1?", "1) v1", "2) v2", "1", "Matematică"), fileName);
        appController.addNewIntrebare(new Intrebare("Enunț 2?", "1) v1", "2) v2", "1", "Română"), fileName);
        appController.addNewIntrebare(new Intrebare("Enunț 3?", "1) v1", "2) v2", "1", "Info"), fileName);
        appController.addNewIntrebare(new Intrebare("Enunț 4?", "1) v1", "2) v2", "1", "Biologie"), fileName);
        appController.addNewIntrebare(new Intrebare("Enunț 5?", "1) v1", "2) v2", "1", "Chimie"), fileName);
        assertEquals(5, appController.getIntrebari().size());
    }

    @Test
    public void integrationTest_TopDown_AB() throws IntrebareValidationFailedException, DuplicateIntrebareException {
        // test Add Intrebare
        appController.addNewIntrebare(new Intrebare("Enunț 1?", "1) v1", "2) v2", "1", "Matematică"), fileName);
        appController.addNewIntrebare(new Intrebare("Enunț 2?", "1) v1", "2) v2", "1", "Română"), fileName);
        appController.addNewIntrebare(new Intrebare("Enunț 3?", "1) v1", "2) v2", "1", "Info"), fileName);
        appController.addNewIntrebare(new Intrebare("Enunț 4?", "1) v1", "2) v2", "1", "Biologie"), fileName);
        appController.addNewIntrebare(new Intrebare("Enunț 5?", "1) v1", "2) v2", "1", "Chimie"), fileName);
        assertEquals(5, appController.getIntrebari().size());

        // test Create Test
        try {
            final evaluator.model.Test test = appController.createNewTest(fileName);
            Assert.assertEquals(5, test.getIntrebari().size());
        } catch (NotAbleToCreateTestException e) {
            Assert.fail();
        }
    }

    @Test
    public void integrationTest_TopDown_ABC() throws IntrebareValidationFailedException, DuplicateIntrebareException {
        // test Add Intrebare
        appController.addNewIntrebare(new Intrebare("Enunț 1?", "1) v1", "2) v2", "1", "Matematică"), fileName);
        appController.addNewIntrebare(new Intrebare("Enunț 2?", "1) v1", "2) v2", "1", "Română"), fileName);
        appController.addNewIntrebare(new Intrebare("Enunț 3?", "1) v1", "2) v2", "1", "Info"), fileName);
        appController.addNewIntrebare(new Intrebare("Enunț 4?", "1) v1", "2) v2", "1", "Biologie"), fileName);
        appController.addNewIntrebare(new Intrebare("Enunț 5?", "1) v1", "2) v2", "1", "Chimie"), fileName);
        assertEquals(5, appController.getIntrebari().size());

        // test Create Test
        try {
            final evaluator.model.Test test = appController.createNewTest(fileName);
            Assert.assertEquals(5, test.getIntrebari().size());
        } catch (NotAbleToCreateTestException e) {
            Assert.fail();
        }

        // test Get Statistica
        try {
            final Statistica statistica = appController.getStatistica();
            Assert.assertNotNull(statistica);
        } catch (NotAbleToCreateStatisticsException e) {
            Assert.fail();
        }
    }
}
