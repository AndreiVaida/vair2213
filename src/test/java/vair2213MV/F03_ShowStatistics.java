package vair2213MV;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.IntrebareValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class F03_ShowStatistics {
    final AppController appController = new AppController();
    private String fileName = "test.txt";

    @Before
    public void init() throws IOException {
        BufferedWriter br = new BufferedWriter(new FileWriter(fileName));
        br.write("");
        br.close();
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void unitTestValid() throws IntrebareValidationFailedException, DuplicateIntrebareException {
        // populate the file with questions in order to do the statistics
        appController.addNewIntrebare(new Intrebare("Întrebare 1?", "1) R1", "2) R2.", "1", "Mate"), fileName);
        appController.addNewIntrebare(new Intrebare("Întrebare 2?", "1) R1", "2) R2.", "2", "Mate"), fileName);
        appController.addNewIntrebare(new Intrebare("Întrebare 3?", "1) R1", "2) R2.", "1", "Info"), fileName);
        appController.addNewIntrebare(new Intrebare("Întrebare 4?", "1) R1", "2) R2.", "2", "Info"), fileName);
        appController.addNewIntrebare(new Intrebare("Întrebare 5?", "1) R1", "2) R2.", "1", "Chimie"), fileName);

        // test
        try {
            final Statistica statistica = appController.getStatistica();
            Assert.assertNotNull(statistica);
            Assert.assertEquals(new Integer(2), statistica.getIntrebariDomenii().get("Mate"));
            Assert.assertEquals(new Integer(2), statistica.getIntrebariDomenii().get("Info"));
            Assert.assertEquals(new Integer(1), statistica.getIntrebariDomenii().get("Chimie"));

        } catch (NotAbleToCreateStatisticsException e) {
            Assert.fail();
        }
    }

    @Test
    public void unitTestInvalid() throws NotAbleToCreateStatisticsException {
        expectedException.expect(NotAbleToCreateStatisticsException.class);
        appController.getStatistica(); // throw exception
        Assert.fail();
    }
}
