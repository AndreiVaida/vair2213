package vair2213MV;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.IntrebareValidationFailedException;
import evaluator.model.Intrebare;
import evaluator.repository.IntrebariRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class F01_AddIntrebare_Test {
    private IntrebariRepository intrebariRepository;
    private String fileName = "test.txt";

    @Before
    public void init() throws IOException {
        intrebariRepository = new IntrebariRepository();
        BufferedWriter br = new BufferedWriter(new FileWriter(fileName));
        br.write("");
        br.close();
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void test01() throws IntrebareValidationFailedException, DuplicateIntrebareException {
        intrebariRepository.addIntrebare(new Intrebare("Întrebare?", "1) R1", "2) R2.", "1", "Mate"), fileName);
        assertEquals(1, intrebariRepository.getIntrebari().size());
    }

    @Test
    public void test02() throws IntrebareValidationFailedException, DuplicateIntrebareException {
        expectedException.expect(IntrebareValidationFailedException.class);
        intrebariRepository.addIntrebare(new Intrebare("întrebare?", "1) R1", "2) R2.", "1", "Mate"), fileName);
        assertEquals(0, intrebariRepository.getIntrebari().size());
    }

    @Test
    public void test03() throws IntrebareValidationFailedException, DuplicateIntrebareException {
        expectedException.expect(IntrebareValidationFailedException.class);
        intrebariRepository.addIntrebare(new Intrebare("", "1) R1", "2) R2.", "1", "Mate"), fileName);
        assertEquals(0, intrebariRepository.getIntrebari().size());
    }

    @Test
    public void test04() throws IntrebareValidationFailedException, DuplicateIntrebareException {
        expectedException.expect(IntrebareValidationFailedException.class);
        intrebariRepository.addIntrebare(new Intrebare("Întrebare?", "R1", "4) R2.", "1", "Mate"), fileName);
        assertEquals(0, intrebariRepository.getIntrebari().size());
    }


    // BVA
    @Test
    public void test05() throws IntrebareValidationFailedException, DuplicateIntrebareException {
        expectedException.expect(IntrebareValidationFailedException.class);
        intrebariRepository.addIntrebare(new Intrebare("Întrebare", "1) R1", "2) R2.", "1", ""), fileName);
        assertEquals(0, intrebariRepository.getIntrebari().size());
    }

    @Test
    public void test06() throws IntrebareValidationFailedException, DuplicateIntrebareException {
        intrebariRepository.addIntrebare(new Intrebare("Întrebare?", "1) R1", "2) R2.", "1", "M"), fileName);
        assertEquals(1, intrebariRepository.getIntrebari().size());
    }

    @Test
    public void test07() throws IntrebareValidationFailedException, DuplicateIntrebareException {
        intrebariRepository.addIntrebare(new Intrebare("Întrebare?", "1) R1", "2) R2.", "1", "Ma"), fileName);
        assertEquals(1, intrebariRepository.getIntrebari().size());
    }

    @Test
    public void test08() throws IntrebareValidationFailedException, DuplicateIntrebareException {
        String domeniu = "";
        for (int i = 0; i < 29; i++) {
            domeniu += "M";
        }
        intrebariRepository.addIntrebare(new Intrebare("Întrebare?", "1) R1", "2) R2.", "1", domeniu), fileName);
        assertEquals(1, intrebariRepository.getIntrebari().size());
    }

    @Test
    public void test09() throws IntrebareValidationFailedException, DuplicateIntrebareException {
        String domeniu = "";
        for (int i = 0; i < 30; i++) {
            domeniu += "M";
        }
        intrebariRepository.addIntrebare(new Intrebare("Întrebare?", "1) R1", "2) R2.", "1", domeniu), fileName);
        assertEquals(1, intrebariRepository.getIntrebari().size());
    }

    @Test
    public void test10() throws IntrebareValidationFailedException, DuplicateIntrebareException {
        expectedException.expect(IntrebareValidationFailedException.class);
        String domeniu = "";
        for (int i = 0; i < 31; i++) {
            domeniu += "M";
        }
        intrebariRepository.addIntrebare(new Intrebare("Întrebare?", "1) R1", "2) R2.", "1", domeniu), fileName);
        assertEquals(0, intrebariRepository.getIntrebari().size());
    }
}
