package vair2213MV;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.IntrebareValidationFailedException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.repository.IntrebariRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.BufferedWriter;
import java.io.FileWriter;

import static org.junit.Assert.assertEquals;

public class F02_CreateNewTest_Test {
    private IntrebariRepository intrebariRepository;
    private AppController appController;
    private String fileName = "test.txt";

    @Before
    public void init() throws Exception {
        intrebariRepository = new IntrebariRepository();
        BufferedWriter br = new BufferedWriter(new FileWriter(fileName));
        br.write("");
        br.close();
        appController = new AppController();
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void test01() throws IntrebareValidationFailedException, DuplicateIntrebareException {
        // init: 5î + 5d
        intrebariRepository.addIntrebare(new Intrebare("Enunț 1?", "1) v1", "2) v2", "1", "Matematică"), fileName);
        intrebariRepository.addIntrebare(new Intrebare("Enunț 2?", "1) v1", "2) v2", "1", "Română"), fileName);
        intrebariRepository.addIntrebare(new Intrebare("Enunț 3?", "1) v1", "2) v2", "1", "Info"), fileName);
        intrebariRepository.addIntrebare(new Intrebare("Enunț 4?", "1) v1", "2) v2", "1", "Biologie"), fileName);
        intrebariRepository.addIntrebare(new Intrebare("Enunț 5?", "1) v1", "2) v2", "1", "Chimie"), fileName);
        assertEquals(5, intrebariRepository.getIntrebari().size());
        assertEquals(5, intrebariRepository.getNumberOfDistinctDomains());
        // test
        try {
            final evaluator.model.Test test = appController.createNewTest(fileName);
            Assert.assertEquals(5, test.getIntrebari().size());
        } catch (NotAbleToCreateTestException e) {
            Assert.fail();
        }
    }

    @Test
    public void test02() throws IntrebareValidationFailedException, DuplicateIntrebareException {
        // init: 4î + 4d
        intrebariRepository.addIntrebare(new Intrebare("Enunț 1?", "1) v1", "2) v2", "1", "Matematică"), fileName);
        intrebariRepository.addIntrebare(new Intrebare("Enunț 2?", "1) v1", "2) v2", "1", "Română"), fileName);
        intrebariRepository.addIntrebare(new Intrebare("Enunț 3?", "1) v1", "2) v2", "1", "Info"), fileName);
        intrebariRepository.addIntrebare(new Intrebare("Enunț 4?", "1) v1", "2) v2", "1", "Biologie"), fileName);
        assertEquals(4, intrebariRepository.getIntrebari().size());
        assertEquals(4, intrebariRepository.getNumberOfDistinctDomains());
        // test
        try {
            appController.createNewTest(fileName);
            Assert.fail();
        } catch (NotAbleToCreateTestException e) {
            assert true;
        }
    }

    @Test
    public void test03() throws IntrebareValidationFailedException, DuplicateIntrebareException {
        // init: 5î + 4d
        intrebariRepository.addIntrebare(new Intrebare("Enunț 1?", "1) v1", "2) v2", "1", "Matematică"), fileName);
        intrebariRepository.addIntrebare(new Intrebare("Enunț 2?", "1) v1", "2) v2", "1", "Română"), fileName);
        intrebariRepository.addIntrebare(new Intrebare("Enunț 3?", "1) v1", "2) v2", "1", "Info"), fileName);
        intrebariRepository.addIntrebare(new Intrebare("Enunț 4?", "1) v1", "2) v2", "1", "Biologie"), fileName);
        intrebariRepository.addIntrebare(new Intrebare("Enunț 5?", "1) v1", "2) v2", "1", "Biologie"), fileName);
        assertEquals(5, intrebariRepository.getIntrebari().size());
        assertEquals(4, intrebariRepository.getNumberOfDistinctDomains());
        // test
        try {
            appController.createNewTest(fileName);
            Assert.fail();
        } catch (NotAbleToCreateTestException e) {
            assert true;
        }
    }

    @Test
    public void test04() throws IntrebareValidationFailedException, DuplicateIntrebareException {
        // init: 9î + 5d
        intrebariRepository.addIntrebare(new Intrebare("Enunț 1?", "1) v1", "2) v2", "1", "Matematică"), fileName);
        intrebariRepository.addIntrebare(new Intrebare("Enunț 2?", "1) v1", "2) v2", "1", "Română"), fileName);
        intrebariRepository.addIntrebare(new Intrebare("Enunț 3?", "1) v1", "2) v2", "1", "Info"), fileName);
        intrebariRepository.addIntrebare(new Intrebare("Enunț 4?", "1) v1", "2) v2", "1", "Biologie"), fileName);
        intrebariRepository.addIntrebare(new Intrebare("Enunț 5?", "1) v1", "2) v2", "1", "Chimie"), fileName);
        intrebariRepository.addIntrebare(new Intrebare("Enunț 6?", "1) v1", "2) v2", "1", "Chimie"), fileName);
        intrebariRepository.addIntrebare(new Intrebare("Enunț 7?", "1) v1", "2) v2", "1", "Chimie"), fileName);
        intrebariRepository.addIntrebare(new Intrebare("Enunț 8?", "1) v1", "2) v2", "1", "Chimie"), fileName);
        intrebariRepository.addIntrebare(new Intrebare("Enunț 9?", "1) v1", "2) v2", "1", "Chimie"), fileName);
        assertEquals(9, intrebariRepository.getIntrebari().size());
        assertEquals(5, intrebariRepository.getNumberOfDistinctDomains());
        // test
        try {
            final evaluator.model.Test test = appController.createNewTest(fileName);
            Assert.assertEquals(5, test.getIntrebari().size());
        } catch (NotAbleToCreateTestException e) {
            Assert.fail();
        }
    }
}
