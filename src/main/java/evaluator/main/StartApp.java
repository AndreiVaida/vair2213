package evaluator.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.IntrebareValidationFailedException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;

import evaluator.controller.AppController;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.model.Test;

//functionalitati
//F01.	 adaugarea unei noi intrebari pentru un anumit domeniu (enunt intrebare, raspuns 1, raspuns 2, raspuns 3, raspunsul corect, domeniul) in setul de intrebari disponibile;
//F02.	 crearea unui nou test (testul va contine 5 intrebari alese aleator din cele disponibile, din domenii diferite);
//F03.	 afisarea unei statistici cu numarul de intrebari organizate pe domenii.

public class StartApp {

	private static final String file = "intrebari.txt";
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
		
		AppController appController = new AppController();
		
		boolean activ = true;
		String optiune = null;
		
		while(activ){
			
			System.out.println("");
			System.out.println("1.Adauga intrebare");
			System.out.println("2.Creeaza test");
			System.out.println("3.Statistica");
			System.out.println("4.Exit");
			System.out.println("");
			
			optiune = console.readLine();
			
			switch(optiune){
			case "1" :
				final Scanner scanner = new Scanner(System.in);
				System.out.println("- Adaugă întrebare -");
				System.out.print("enunț: ");
				final String enunț = scanner.nextLine();
				System.out.print("varianta 1: ");
				final String varianta1 = scanner.nextLine();
				System.out.print("varianta 2: ");
				final String varanta2 = scanner.nextLine();
				System.out.print("varianta corectă: ");
				final String variantaCorectă = scanner.nextLine();
				System.out.print("domeniu: ");
				final String domeniu = scanner.nextLine();
				try {
					appController.addNewIntrebare(new Intrebare(enunț, varianta1, varanta2, variantaCorectă, domeniu), file);
				} catch (DuplicateIntrebareException e) {
					System.out.println("Întrebarea deja există.");
				} catch (IntrebareValidationFailedException e) {
					System.out.println("Nu s-a salvat întrebarea: " + e.getMessage());
				}
				break;
			case "2" :
				try {
					final Test test = appController.createNewTest(file);
					System.out.println(test.toString());
				} catch (NotAbleToCreateTestException e) {
					System.out.println("Nu s-a creat testul: " + e.getMessage());
				}
				break;
			case "3" :
				appController.loadIntrebariFromFile(file);
				Statistica statistica;
				try {
					statistica = appController.getStatistica();
					System.out.println(statistica);
				} catch (NotAbleToCreateStatisticsException e) {
					System.out.println("Nu s-a creat statistica: " + e.getMessage());
				}
				
				break;
			case "4" :
				activ = false;
				break;
			default:
				break;
			}
		}
		
	}

}
