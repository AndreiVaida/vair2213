import React from 'react';
import Button from 'react-bootstrap/Button'
import './App.css';

function App() {
  return (
      <AddQuestion/>
  );
}

class AddQuestion extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      enunt: '',
      varianta1: '',
      varianta2: '',
      variantaCorecta: '',
      domeniu: '',
      textToShow: '',
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.getTextFromServer = this.getTextFromServer.bind(this);
  }
  întrebareUrl = "http://localhost:8080/intrebare";

  handleSubmit(event) {
    const întrebare = {
      enunt: this.state.enunt,
      varianta1: this.state.varianta1,
      varianta2: this.state.varianta2,
      variantaCorecta: this.state.variantaCorecta,
      domeniu: this.state.domeniu
    };
    console.log(întrebare);
    this.sendÎntrebareToServer(întrebare);
    event.preventDefault();
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  sendÎntrebareToServer(întrebare) {
    fetch(this.întrebareUrl, {
      method: 'POST',
      headers: {'Content-Type':'application/json'},
      body: JSON.stringify(întrebare)
    })
        .then(res => res.text())
        .then(
            (result) => {
              this.setState({
                textToShow: result,
              });
            },
            // Note: it's important to handle errors here
            // instead of a catch() block so that we don't swallow
            // exceptions from actual bugs in components.
            (error) => {
              this.setState({
                textToShow: "EROARE",
              });
            }
        );
  }

  getTextFromServer() {
    console.log(this.întrebareUrl);
    fetch(this.întrebareUrl)
        .then(res => res.text())
        .then(
            (result) => {
              this.setState({
                textToShow: "SUCCES ! " + result,
              });
            },
            // Note: it's important to handle errors here
            // instead of a catch() block so that we don't swallow
            // exceptions from actual bugs in components.
            (error) => {
              console.log(error);
              this.setState({
                textToShow: "EROARE" + error,
              });
            }
        )
  }

  render() {
    return (
        <div className="App">
          <form onSubmit={this.handleSubmit}>
            <label>
              Enunț:
              <input type="text" name="enunt" value={this.state.enunt} onChange={this.handleChange} />
            </label><br/>
            <label>
              Varianta 1:
              <input type="text" name="varianta1" value={this.state.varianta1} onChange={this.handleChange} />
            </label><br/>
            <label>
              Varianta 2:
              <input type="text" name="varianta2" value={this.state.varianta2} onChange={this.handleChange} />
            </label><br/>
            <label>
              Varianta corectă:
              <input type="text" name="variantaCorecta" value={this.state.variantaCorecta} onChange={this.handleChange} />
            </label><br/>
            <label>
              Domeniu:
              <input type="text" name="domeniu" value={this.state.domeniu} onChange={this.handleChange} />
            </label><br/>
            <input type="submit" id="addQuestionButton" value="Adaugă întrebare" />
          </form>

          <div id="textToShow">{this.state.textToShow}</div>

          <Button variant="primary" onClick={this.getTextFromServer}>Get some text from server !</Button>

        </div>
    );
  }
}

export default App;
