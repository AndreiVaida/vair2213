package evaluator.controller;

import evaluator.model.Intrebare;
import evaluator.model.IntrebareDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("")
public class WebController {
    private final AppController appController;
    private final String fileName;

    public WebController() {
        appController = new AppController();
        fileName = "intrebariWeb.txt";
    }

    @GetMapping("intrebare")
    public String getSomeText() {
        return "Bine ai venit !";
    }

    @PostMapping("intrebare")
    public String addÎntrebare(@RequestBody final IntrebareDto intrebareDto) {
        try {
            final Intrebare intrebare = new Intrebare(intrebareDto.getEnunt(), intrebareDto.getVarianta1(), intrebareDto.getVarianta2(), intrebareDto.getVariantaCorecta(), intrebareDto.getDomeniu());
            appController.addNewIntrebare(intrebare, fileName);
            return "Succes !";

        } catch (Exception e) {
            return "Eșuat: " + e.getMessage();
        }
    }
}
