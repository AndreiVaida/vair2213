package evaluator.exception;

public class IntrebareValidationFailedException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public IntrebareValidationFailedException(String message){
		super(message);
	}

}
